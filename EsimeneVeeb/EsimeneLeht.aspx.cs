﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EsimeneVeeb
{
    public partial class EsimeneLeht : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            northwindEntities ne = new northwindEntities();
            EmployeeList.DataSource =
            ne.Employees.Select(x => new { x.FirstName, x.LastName}).ToList();
            EmployeeList.DataBind();
        

            ProductList.DataSource =
            ne.Products.Select(x => new { x.ProductID, x.ProductName, x.UnitPrice }).ToList();
            ProductList.DataBind();

            

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            this.Label1.Text = $"Tere {this.TextBox1.Text}!";

        }
    }
}